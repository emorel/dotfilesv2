" Use system clipboard
set clipboard=unnamedplus
" Enable mouse
set mouse=a
" Highlight current line
set cursorline
" Add minimum number of lines at between cursor and screen end
set scrolloff=99
" Show invisible
set list!

" Block folding
set foldmethod=syntax
set foldlevel=99
" Fold with space bar
noremap <silent> <space> za

set splitright
set splitbelow

" Tabs
set tabstop=2
set shiftwidth=2
set softtabstop=2
set autoindent
set expandtab
set smarttab

" Highlights columns 80 and 120
set colorcolumn=80,120

" Auto detect syntax missing file types
autocmd BufReadPost *dockerfile* set syntax=dockerfile
" Auto delete trailing whitespaces
autocmd BufWritePre * %s/\s\+$//e

" Set cursor back to previous position when reopening a file
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$")
    \ | exe "normal! g'\"" |
  \ endif

" ---------------------------
"       NUMBER STUFF
" ---------------------------
set number

augroup AutoNumberToggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter *
    \ if &number==1
      \ | set norelativenumber |
    \ endif
augroup END

" -----------------------------
"           PLUGINS
" -----------------------------
call plug#begin("~/.vim/plugged")
  " Language plugins
        Plug 'vim-ruby/vim-ruby',       { 'for': ['ruby']  }
        Plug 'vim-crystal/vim-crystal', { 'for': ['crystal']  }
        Plug 'rust-lang/rust.vim',      { 'for': ['rust']  }
  Plug 'slim-template/vim-slim',  { 'for': ['slim'] }
  Plug 'ekalinin/Dockerfile.vim', { 'for': ['dockerfile'] }

  Plug 'neoclide/coc.nvim', { 'branch': 'release'  }

        " Framework plugins
        " Plug 'tpope/vim-rails'

        " File management plugins
        Plug 'preservim/NERDTree'
        Plug 'junegunn/fzf', { 'do': { -> fzf#install()  }  }
        Plug 'junegunn/fzf.vim'

        " QoL plugins
        Plug 'jiangmiao/auto-pairs'
        Plug 'tpope/vim-endwise', { 'for': ['ruby', 'crystal']  }

        " Theme plugins
        Plug 'joshdick/onedark.vim'
        Plug 'morhetz/gruvbox'

        " Plug 'ryanoasis/vim-devicons' " Custom icons for file types
  Plug 'yggdroot/indentline'

  Plug 'itchyny/lightline.vim'
        "Plug 'mengelbrecht/lightline-bufferline'
call plug#end()

" Use alt+hjkl to move between split planels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" --------------------------------
"       INTEGRATED TERMINAL
" --------------------------------
autocmd TermOpen * startinsert
tnoremap <ESC> <C-\><C-n>
nnoremap <C-n> :call OpenTerminal()<CR>

function! OpenTerminal()
  split term://bash
  resize 10
  setlocal nonumber norelativenumber
endfunction

" --------------------------------
"           GRUVBOX
" --------------------------------

set termguicolors
set background=dark

let g:gruvbox_contrast_dark='medium'
let g:gruvbox_italic=1

colorscheme gruvbox

" ---------------------------------
"           LIGHTLINE
" ---------------------------------

" Handled by lightline
set noshowmode

let g:lightline = {
    \ 'colorscheme': 'gruvbox'
  \ }

" ---------------------------------
"           BUFFERLINE
" ---------------------------------

"let g:lightline.component_expand = { 'buffers': 'lightline#bufferline#buffers' }
"let g:lightline.component_type = { 'buffers': 'tabsel' }
"let g:lightline.tabline = { 'left': [['buffers']], 'right': [['tabs']] }
"let g:lightline.tab = {
"    \ 'active': ['tabnum', 'modified'],
"    \ 'inactive': ['tanum', 'modified']
"  \ }
"
"autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()
"
"let g:lightline#bufferline#unicode_symbol=1
"let g:lightline#bufferline#min_buffer_count=2
"
" Use Tab to navigate between tabs
noremap <silent> <Tab> :bnext<CR>
noremap <silent> <S-Tab> :bprev<CR>

" ---------------------------------
"         INDENT LINE
" ---------------------------------

" Let IndentLine set the colors with conceal
let g:indentLine_setColors = 1
" Each character is an independent indentation level
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" ---------------------------------
"           NERD TREE
" ---------------------------------
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1

noremap <C-b> :NERDTreeToggle<CR>

" ---------------------------------
"             COC
" ---------------------------------
let g:coc_global_extensions = ['coc-emmet', 'coc-css', 'coc-html', 'coc-json', 'coc-solargraph']
let g:coc_status_warning_sign = "\uf071 "
let g:coc_status_error_sign = "\uf05e "

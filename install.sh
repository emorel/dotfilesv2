#!/bin/bash

# This file installs all my usual packages

sudo apt update

## Various dependencies
sudo apt-get install cmake \
  bc \
  checkinstall \
  cmake-data \
  imagemagick \
  libasound2-dev \
  libcairo2-dev \
  libcurl4-openssl-dev \
  libdbus-1-dev \
  libev-dev \
  libglib2.0-dev \
  libgtk2.0-dev \
  libiw-dev \
  libjpeg-turbo8-dev \
  libmpdclient-dev \
  libpam0g-dev \
  libpango1.0-dev \
  libpulse-dev \
  libx11-dev \
  libxcb-composite0 \
  libxcb-composite0-dev \
  libxcb-ewmh-dev \
  libxcb-ewmh2 \
  libxcb-icccm4-dev \
  libxcb-image0-dev \
  libxcb-randr0 \
  libxcb-randr0-dev \
  libxcb-util-dev \
  libxcb-util0-dev \
  libxcb-xinerama0 \
  libxcb-xinerama0-dev \
  libxcb-xkb-dev \
  libxcb-xrm-dev \
  libxcb1-dev \
  libxdg-basedir-dev \
  libxinerama-dev \
  libxkbcommon-x11-dev \
  libxrandr-dev \
  libxss-dev
  pkg-config \
  python-xcbgen \
  xcb \
  xcb-proto \

# Packages
sudo apt-get install rxvt-unicode \
  compton \
  conky \
  feh \
  htop \
  neofetch \
  ranger \
  rofi \
  wmctrl \
  xdotool \

Polybar
# Clone repo
git clone https://github.com/jaagr/polybar
## Install
cd polybar && ./build.sh
## Clean up
cd ..
rm -rf polybar/

# Dunst notification service
## Clone repo
git clone https://github.com/dunst-project/dunst.git
## Install
cd dunst && make -j5
sudo make install
cd ..
rm -rf dunst/
## Pseudo-dependency to send messages to dunst
sudo apt install libnotify-bin

# Better lock screen
## Install i3lock-color dependency
git clone https://github.com/PandorasFox/i3lock-color && cd i3lock-color;
autoreconf -i; ./configure;
make; sudo checkinstall --pkgname=i3lock-color --pkgversion=1 -y;
cd .. && sudo rm -rf i3lock-color;

# Bootstrap config
./bootstrap.sh

#!/bin/bash

initDotfiles() {
	rsync --exclude ".git/" \
		--exclude "bootstrap.sh" \
		--exclude "install.sh" \
		--exclude "README.md" \
    --exclude "betterlockscreen.sh" \
    --exclude "tmp/" \
		-avh . ~;
	source ~/.bash_profile
}

# RSync dotfiles
initDotfiles;
# Force i3 to use new config
i3 restart;
# Reload any theming changes
xrdb ~/.Xresources;

unset initDotfiles;

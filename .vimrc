set tabstop=2
set shiftwidth=2
set laststatus=2
set noshowmode
set nu

autocmd InsertEnter,InsertLeave * set cul!

call plug#begin()
" Language plugins
Plug 'vim-ruby/vim-ruby', { 'for': ['ruby']  }
Plug 'vim-crystal/vim-crystal', { 'for': ['crystal']  }
Plug 'rust-lang/rust.vim', { 'for': ['rust']  }

" Framework plugins
Plug 'tpope/vim-rails' 

" File management plugins
Plug 'preservim/NERDTree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install()  }  }
Plug 'junegunn/fzf.vim'

" QoL plugins
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-endwise', { 'for': ['ruby', 'crystal']  }

" Theme plugins
Plug 'joshdick/onedark.vim'
Plug 'itchyny/lightline.vim'

call plug#end()

" ================
" UI CUSTOMIZATION
" ================
"
" May require some themes to be installed and plugged first so keep this after
" all Plug calls
colorscheme onedark
syntax on

" ================
" LIGHTLINE CONFIG
" ================
let g:lightline = { 'colorscheme': 'onedark'  }

" ===============
" NERDTREE CONFIG
" ===============
let NERDTreeShowHidden=1
let NERDTreeQuitOnOpen=1

map <C-n> :NERDTreeToggle<CR>

" ==========
" FZF CONFIG
" ==========
map <C-p> :Files<CR> 
" :Ag requires ag to be installed on the system
if executable('ag')
        map <C-f> :Ag! 
endif